class Customer{
    constructor(email, cart){
        this.email = email
        this.cart = cart
        this.orders = []
    }
    checkOut(){
        if(this.cart.contents.length == 0){
            console.log("There are no items in your cart!")
            return this
        }else{
            this.orders.push({products: this.cart.contents, totalAmount: this.cart.computeTotal().totalAmount})
            this.cart.clearCartContents()
            return this
        }
    }
}

class Cart{
    constructor(){
        this.contents = []
        this.totalAmount = 0
    }
    addToCart(x, y){
        this.contents.push({product: x, quantity: y})
        return this
    }
    showCartContents(){
        console.log(this.contents)
        return this
    }
    updateProductQuantity(a, b){
        let x = 0
        let y = undefined

        do{
            y = this.contents[x].product.name
            if(y == a){
                this.contents[x].quantity = b
                x++
            } else{x++}
        } while (x < this.contents.length)

        if (y != a){
            console.log(`error, ${a} couldn't be found in cart`)
            return this
        } else{
            return this
        }
        
    }
    clearCartContents(){
        this.contents = []
        this.totalAmount = 0
        return this
    }
    computeTotal(){
        let x = 0
        let y = 0

        if(this.contents.length != 0){
            do{
                y += this.contents[x].product.price * this.contents[x].quantity
                x++
            } while (x < this.contents.length)
    
            this.totalAmount = y
            return this
        } else{
            console.log("There are no items in your cart!")
            return this
        }
    }       
}

class Product{
    constructor(name, price){
        this.name = name
        this.price = price
        this.isActive = true
    }
    archive(){
        if(this.isActive == true){
            this.isActive = false
            return this
        }
        else{
            this.isActive = true
            return this
        }
    }
    updatePrice(updatedPrice){
        this.price = updatedPrice
        return this
    }
}


let product1 = new Product("headphones", 500)
let product2 = new Product("vape", 800)
let product3 = new Product("subwoofer", 1200)

let cart1 = new Cart()

cart1.addToCart(product1, 3)

let customer1 = new Customer("customer@mail.com", cart1)